This code will search your entire CloudFlare account for any matching ip address and change them one at a time or all at once.

Uses the CloudFlare API and requires no other libraries.


This code was originally found here:
    https://www.aetherweb.co.uk/batch-editing-cloudflare-dns-ip-with-cloudflare-api-in-php/
    
I fixed a bug or two and added command line parameters to make it more useful.

This is to be run ONLY as a CLI! :)

Works great for me!


Usage:
   --cfemail=<cloudflare email address>
   --cfapikey=<cloudflare api key>
   --from=<ip address to replace>
   --to=<target ip address>
   --doit=1  (default 0)
   --confirm=0 (default 1)
   

Example: 

  run it without doing any replacments (dry run)
  php update.php --cfemail=<your_cf_email_goes_here> --cfapikey=<your_cf_api_key_goes_here> --from=1.1.1.1 --to=9.9.9.9
  
  do the replacements - will confirm each change
  php update.php --cfemail=<your_cf_email_goes_here> --cfapikey=<your_cf_api_key_goes_here> --from=1.1.1.1 --to=9.9.9.9 --doit=1 
  
  do the replacements without any confirmation
  php update.php --cfemail=<your_cf_email_goes_here> --cfapikey=<your_cf_api_key_goes_here> --from=1.1.1.1 --to=9.9.9.9 --doit=1 --confirm=0