<?php
// Form a list of all CF IP zones 
// For each zone, grab all A records and TXT records matching $oldip
// For each matching record, update it to the new IP address
// original code from 
// lots of cool updates by pmsteil


/************************************************************************************/
/* process command line args                                                        */
/************************************************************************************/
$shortopts  = "";
//$shortopts .= "v::"; // Optional value

$longopts  = array(
    "cfemail:",     // Required value
    "cfapikey:",     // Required value
    "from:",     // Required value
    "to:",     // Required value
    "doit::",    // Optional value
    "confirm::",    // Optional value
    "v::"    // Optional value    
);

$options = getopt($shortopts, $longopts);

if( !isset( $options['cfemail']) || !isset( $options['cfapikey'])  || !isset( $options['from']) || !isset( $options['to']) ){
    usage();
}

$authemail              = $options['cfemail'];
$authkey                = $options['cfapikey'];
$oldip                  = $options['from'];
$newip                  = $options['to'];
$confirmbeforereplace   = 1; // default 1
$verboseoutput          = 1; // default 0
$actuallydoit           = 0;  // default 0


if( isset( $options['v'] )){
    if ($options['v'] == 0 ){
        $verboseoutput = 0;
    }
    
}

if( isset( $options['doit'] )){
    if ($options['doit'] == 1 ){
        $actuallydoit = 1;
    } 
}

if( isset( $options['confirm'] )){
    if ($options['confirm'] == 0 ){
        $confirmbeforereplace = 0;
    } 
}

//if we are not changing them, force to display which ones would get changed
if( $actuallydoit == 0 ){
    $verboseoutput = 1;
}




/************************************************************************************/
/* tell user what is going to happen                                                */
/************************************************************************************/

if( $actuallydoit ){
    echo "Searching Cloudflare domains for [$oldip] to REPLACE with [$newip]\n";
    echo ("REPLACEMENTS WILL BE MADE\n");
    if( $confirmbeforereplace ){
        echo ("You will be prompted before each change is made\n");
    }
    
} else {
    echo "Searching Cloudflare domains for [$oldip] to REPLACE with [$newip] - NO REPLACEMENTS WILL BE MADE, use --doit=1 option to make changes\n";
}










/************************************************************************************/
/* begin reviewing domains                                                          */
/************************************************************************************/

$domainsfound = 0;

//get list of domains from cloudflare
$ch = curl_init("https://api.cloudflare.com/client/v4/zones?page=1&per_page=1000&match=all");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
	    'X-Auth-Email: '.$authemail,
	    'X-Auth-Key: '.$authkey,
	    'Content-Type: application/json'
	    ));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
$response = curl_exec($ch);
curl_close($ch);

//decode the json returned from cloudflare
$r = json_decode($response, true);
$result = $r['result'];


$count = 1; // keep track of how many we find that need to be updated

//loop over all domains returned by cloudflare
foreach ($result as $zone)
{
    if (isset($zone['id']))
    {
        $zoneid   = $zone['id'];
        $zonename = $zone['name'];
        
        printf( "searching " . $zonename . "                                                                      \r"); 
        
	    $count++;

        // get all DNS records for this domain
        $ch = curl_init("https://api.cloudflare.com/client/v4/zones/$zoneid/dns_records");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'X-Auth-Email: '.$authemail,
            'X-Auth-Key: '.$authkey,
            'Content-Type: application/json'
          ));
			
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);
        curl_close($ch);
        $r = json_decode($response, true);

        $dnsrecs = $r['result'];

        //loop over each dns record returned
        foreach ($dnsrecs as $dns)
        {
            //look for an exact match on IP
            if ( $dns['content'] == $oldip ) 
            {
                //we found a match!
                $domainsfound++;
                // OK! Change the data
                $newcontent =$newip;
                                
                if( $verboseoutput ){
                    echo $dns['name'];
                    echo "       --> " . $dns['content'];
                    echo " / $newcontent                                           \n";
                } else {
                    echo $dns['name'] . "                                          \n";
                }

                // Swap the content then
                $dns['content'] = $newcontent;
                
                if( $actuallydoit ){
                    $verify = "y";
                    if( $confirmbeforereplace ){
                        $verify = strtolower(readline("Replace $oldip with $newip for " . $dns['name']. "? (y/n)"));
                    }
                    
                    if( $verify == "y" ){
                        //call this function to update the cloudflare record
                        updateDNSRecord($dns);
                    }
                }
            }
        }
        echo "Found $domainsfound domains... ";
    }
    
}
echo "\n";



function updateDNSRecord($dns)
{
    global $authemail, $authkey;
    $ch = curl_init("https://api.cloudflare.com/client/v4/zones/".$dns['zone_id']."/dns_records/".$dns['id']);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'X-Auth-Email: '.$authemail,
        'X-Auth-Key: '.$authkey,
        'Content-Type: application/json'
      ));

    $data_string = json_encode($dns);
    
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $response = curl_exec($ch);
    curl_close($ch);
    $r = json_decode($response, true);

    //print_r($r['success']);
    if( $r['success'] == 1){
        echo "replace successful... for " . $dns['name'] . "\n";
    } else {
        echo "REPLACE NOT SUCCESSFUL... for " . $dns['name'] . "\n";
        print_r( $r );
    }
    //print "<br />\n";
}



function usage(){
    
    echo "\n";
    echo "Usage:\n";
    echo "   --cfemail=<cloudflare email address>\n";
    echo "   --cfapikey=<cloudflare api key>\n";
    echo "   --from=<ip address to replace>\n";
    echo "   --to=<target ip address>\n";
    echo "   --doit=1  (default 0)\n";
    echo "   --confirm=0 (default 1)\n";
    echo "   --v=1  (default 0)\n";
    
    
    echo "\n";
    echo "Example: \n";
    echo "  php update.php --cfemail=<yourcfemail> --cfapikey=<yourapikey> --from=1.1.1.1 --to=9.9.9.9 --doit=0 --confirm=1\n";
    echo "\n";
    die;
}



?>



